const express = require('express');
const app = express();
const port = 3000;
const config = require('./company.json');
var time = 3000;

// handle CORS policy
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/companies', (req, res) => {
  setTimeout(function(){
    res.send(config);
  }, time);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
